package com.marcfarssac.lecture3.utils;

import android.util.Log;

import com.marcfarssac.lecture3.BuildConfig;

public class Logger {

    public void d(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message);
    }

    public void e(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.e(tag, message);
    }

    public void i(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.i(tag, message);
    }

    public void v(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.v(tag, message);
    }

    public void w(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.w(tag, message);
    }

}
