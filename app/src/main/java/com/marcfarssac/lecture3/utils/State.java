package com.marcfarssac.lecture3.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class State {

    private List<String> statesOrder = Arrays.asList("first", "second", "third", "fourth", "fifth", "sixth", "seventh or latter");
    private List<String> visitedState;

    public State() {
        visitedState = new ArrayList<String>();
    }

    public boolean wasVisited(String stateName) {

        if (!visitedState.contains(stateName)) visitedState.add(stateName);
        else return true;

        return false;
    }

    public String getStateOrder(String stateName){

        int index = visitedState.indexOf(stateName);
        if (index>=statesOrder.size())
            return statesOrder.get(statesOrder.size()-1);

        return statesOrder.get(index);
    }
}
