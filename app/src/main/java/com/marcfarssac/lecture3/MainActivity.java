package com.marcfarssac.lecture3;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.marcfarssac.lecture3.utils.Logger;
import com.marcfarssac.lecture3.utils.State;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "StateOrderApp";
    private State state = new State();
    private String currentState;
    private Logger log = new Logger();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        currentState = this.getLifecycle().getCurrentState().name();
        if(!state.wasVisited(currentState)) log.d(TAG, state.getStateOrder(currentState) + " is " + currentState);
    }
}
